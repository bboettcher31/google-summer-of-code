# Google Summer of Code at SAT

This repository contains project ideas and proposals for the Google Summer of Code at the [Society for Arts and Technology](https://sat.qc.ca/).

The static website is generated with [nikola](https://getnikola.com/) and deployed to gitlab pages using [a gitlab CI configuration](.gitlab-ci.yml)

## Installation
### Recommended installation process (python3 venv)

Use python3 venv in the `nikola-venv` directory at the root of this repo

```bash
sudo apt install python3-venv
python3 -m venv nikola-venv
cd nikola-venv/
source bin/activate
bin/python -m pip install -U pip setuptools wheel
bin/python -m pip install -U "Nikola[extras]"
cd ..
```
Done !

### Using venv

Before running the nikola command, activate the python3 venv :
```bash
source nikola-venv/bin/activate
```
## Browse the website locally before pushing to gitlab

From the website folder, run `nikola auto --browser` to build and run the server automatically, detect site changes and rebuild, and refresh the browser:
    
```
cd website
nikola auto --browser
```

This should open the website in your default browser. If not, use this url : `http://127.0.0.1:8000/`

## More information about nikola

[Getting started](https://getnikola.com/getting-started.html) (10 min. read)

## Contributing
### Project ideas

Copy [idea-template.md](idea-template.md) into the [2022-ideas](website/posts/2022-ideas) folder:
```
cp idea-template.md website/posts/2022-ideas/idea-title.md
```

Then rename `idea-title.md` by replacing `title` by a dash-separated list of lowercase words.

Checkout to a new branch named after the filename without extension, as in `idea-title`.

Write your project idea, commit and push your contributions.

Open a merge request from your branch on https://gitlab.com/sat-mtl/google-summer-of-code/-/merge_requests

### Proposals

First [fork this repository](https://gitlab.com/sat-mtl/google-summer-of-code/-/forks/new) into a public fork using your account as namespace.

Clone your fork into your computer.

In your clone, copy [proposal-template.md](proposal-template.md) into the [2022-proposals](website/posts/2022-proposals) folder:
```
cp proposal-template.md website/posts/2022-proposals/proposal-username-title.md
```

Then rename `proposal-username-title.md` by replacing `username` by your gitlab username and `title` by your proposal title, the whole filename should be a dash-separated list of lowercase words. 

Checkout to a new branch named after the filename without extension.

Edit your proposal file and follow the instructions formatted as inline HTML comments.

While you write your proposal, regularly commit and push your contributions.

From the online gitlab.com landing page of your fork, navigate to the merge request section to open a merge request from your branch to the main branch of this repository so that we can discuss your proposal idea. 

Google requires proposals to be submitted through their online forms, including a PDF file. Please check our [proposal forms walkthrough](website/posts/2022-proposals/proposal-forms.md). We automate the production of a PDF file for each proposal in markdown format through our [gitlab CI config](.gitlab-ci.yml). These PDF files are available as artifacts from our [gitlab CI pipelines](https://gitlab.com/sat-mtl/google-summer-of-code/-/pipelines). To test the production of PDF files locally, with Ubuntu 20.04 LTS, please first run `./install.sh` then `./build-proposal.sh website/posts/2022-proposals/proposal-welcome.md` with the filename adapted to your proposal. 

### Communication

From: https://developers.google.com/open-source/gsoc/resources/marketing#template_email_for_gsoc_applicants

> Do you know anyone who might be a good candidate to participate as a contributor in GSoC? You can use the sample email below to help advertise the program. Consider sending it to appropriate schools, teachers, friends, clubs, developer communities, etc.

Please use this communication template adapted for SAT: [website/posts/2022-share.md](website/posts/2022-share.md)
