## Application idea

Each potential GSoC contributor should open one issue per application idea to discuss.

### Is your application informed by rules and guides?

To discuss application ideas with the SAT, please first complete these tasks (mandatory):

- [ ] confirm that you are eligible to participate as a student contributor by reading section 7. GSoC Contributors of GSoC rules: https://summerofcode.withgoogle.com/rules
    - [ ] I am eighteen (18) years of age or older upon registration for the Program.
    - [ ] I am eligible to work in the country in which I reside for the duration of the Program.
    - [ ] I am **not** an Organization Administrator or Mentor in the Program. 
    - [ ] I am **neither** a resident of a United States embargoed country; **nor** ordinarily resident in a United States embargoed country; otherwise prohibited by applicable export controls and sanctions programs. 
    - [ ] I am **not** an employee (including intern), contractor, officer, or director of: Google or its affiliates, or an Organization or any of its affiliates. 
    - [ ] I am **not** an immediate family member (including a parent, sibling, child, spouse, or life partner) of a Mentor or Organization Administrator with the same Organization or a member of the same household (whether related or not) as a Mentor or Organization Administrator with the same Organization.
    - [ ] I have **not** previously participated as a GSoC Contributor in Google Summer of Code two (2) or more times.
    - [ ] I am **not** not a new or beginner contributor to open source coding projects, but I may be a person who was an accepted GSoC student in 2020 or 2021 can apply for a second time to be a GSoC Contributor for this Program. 
- [ ] confirm that you are available during the GSoC timeline: https://developers.google.com/open-source/gsoc/timeline 
- [ ] read the GSoC contributor guide: https://google.github.io/gsocguides/student/
- [ ] browse our project ideas with the SAT: https://sat-mtl.gitlab.io/google-summer-of-code/categories/ideas/
- [ ] read our proposals guide with the SAT: https://sat-mtl.gitlab.io/google-summer-of-code/posts/2022-proposals/proposal-welcome/

### Does your application fit to an existing project idea or require a new project idea?

Please choose option 1. or 2.

If your application idea is close to an existing project idea, please check the following box and add a link which [project idea](https://sat-mtl.gitlab.io/google-summer-of-code/categories/ideas/) you are interested in. 

- [ ] 1. I am interested in discussing this project idea: link project idea

If your application idea is not close to an existing project idea, check the following box, we will pair you with our GSoC mentors to [submit a new project idea](https://sat-mtl.gitlab.io/google-summer-of-code/posts/2022-ideas/idea-welcome/).

- [ ] 2. I would like to propose a new project idea before starting a proposal.

### Does your application target specific GSoC mentors?

If you have identified (a) specific mentor(s) that could help you discuss your application idea (you will find their GitLab usernames in project idea pages), please check the following box and mention their GitLab usernames.

- [ ] (optional) I would like to discuss particularly with the following GSoC mentors: mention gitlab username(s)

Otherwise, we will assign mentors to lead the discussion.

### How did you find out about GSoC with the SAT?

Please check the source that best applies:
- [ ] Google Summer of Code Organization List: https://summerofcode.withgoogle.com/organizations/
- [ ] SAT LinkedIn: https://www.linkedin.com/company/societe-des-arts-technologiques/
- [ ] SAT Twitter: https://twitter.com/SATmontreal
- [ ] SAT Website: https://sat.qc.ca/
- [ ] Other: please specify: ...

/label ~application-idea
